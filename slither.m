%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% slither.m
%
% The MATLAB portion of Project: SNEK that will display the results and whatnot, maybe even have a snake game
%
% By Mike 'AndroidKitKat', Ben, Noah, and Jacob
%
% A fork of https://github.com/58402140/Snake with improvements!
%
% Thanks for playing...
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;
close all;
